package com.Aina.Android;

import android.app.Activity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

public class Test_SeekBar extends Activity implements SeekBar.OnSeekBarChangeListener{
    /** Called when the activity is first created. */
	private SeekBar seekBar;
	private TextView textView1,textView2;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        seekBar = (SeekBar) this.findViewById(R.id.SeekBar01);
        textView1 = (TextView) this.findViewById(R.id.TextView1);
        textView2 = (TextView) this.findViewById(R.id.TextView2);
        seekBar.setOnSeekBarChangeListener(this);//添加事件监听
    }
    //拖动中
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		this.textView1.setText("当前值:"+progress);
		
	}
	//开始拖动
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		this.textView2.setText("拖动中...");
		
	}
	//结束拖动
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		this.textView2.setText("拖动完毕");
		
	}
}